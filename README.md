# Proyecto Prueba 2



#include <Keypad.h>
#include <LiquidCrystal.h>


int RS=7, E=6, D4=5, D5=4, D6=3, D7=2;
LiquidCrystal lcd(RS,E,D4,D5,D6,D7);

const byte ROWS = 4; //Filas
const byte COLS = 3; //Columnas

//Definimos los simbolos a utilizar (Codigo ask)
char hexaKeys[ROWS][COLS] = 
{
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'*','0','#'}
};

byte rowPins[ROWS] = {8,A0,A1,A2};  //Conectamos las filas al Keypad
byte colPins[COLS] = {A3,A4,A5};   //Conectamos las columnas al Keypad

Keypad customKeypad = Keypad( makeKeymap (hexaKeys), rowPins, colPins, ROWS, COLS); //Mapeo


char Tecla;
int A;
void setup() 
{
  lcd.begin(16,2);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);

}

int BRILLO=0;
void loop()
 
{
  Tecla = customKeypad.getKey();
  switch (Tecla)
  {
    case '1':
    lcd.setCursor(0,0);
    lcd.print("LED VERDE     ");
    lcd.setCursor(0,1);
    lcd.print("              ");
    A = 9;
    break;

    case '2':
    lcd.setCursor(0,0);
    lcd.print("LED AMARILLO ");
    lcd.setCursor(0,1);
    lcd.print("              ");
    A = 10;
    break;

    case '3':
    lcd.setCursor(0,0);
    lcd.print("LED ROJO     ");
    lcd.setCursor(0,1);
    lcd.print("              ");
    A = 11;
    break;

    
    case '*':
    if (BRILLO <=254)
    {
      BRILLO++;
    }
    lcd.setCursor(0,1);
    lcd.print("BRILLO: "), lcd.print(BRILLO);
    analogWrite(A, BRILLO);
    delay(15);
    break;

   
    case '#':
    if (BRILLO >=1)
    {
      BRILLO--; 
    }
    lcd.setCursor(0,1);
    lcd.print("BRILLO: "), lcd.print(BRILLO);
    analogWrite(A, BRILLO);
    delay(15);
    break;
  }
}



